package main

import (
	"encoding/json"
	"errors"
	//"flag"
	"fmt"
	"io/ioutil"
	"math"
	"reflect"
	"strconv"
)


type ParentShape struct {
	Type  string  `json:"type"`
	R interface{} `json:"r"`
	A interface{} `json:"a"`
	B interface{} `json:"b"`
	C interface{} `json:"c"`
}
//
//type Shape interface {
//	Area() float64
//}
type Shapes struct {
	Shapes []ParentShape `json:"figures"`
}
func Parse(item interface{}) (float64, error) {
		reflectType := reflect.TypeOf(item)
		reflectValue := reflect.ValueOf(item)
		switch reflectType.Kind() {
		case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
			return float64(reflectValue.Int()), nil
		case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
			return float64(reflectValue.Uint()), nil
		case reflect.Float32, reflect.Float64:
			return reflectValue.Float(), nil
		case reflect.String:
			val, err := strconv.ParseFloat(reflectValue.String(), 64)
			if err != nil {
				return -1, err
			}
			return val, nil
		default:
			return -1, errors.New("-1")
		}
}
type Rectangle struct{
	a float64
	b float64
}
type Circle struct{
	radius float64
}
type Triangle struct{
	a float64
	b float64
	c float64
}
type Square struct{
	a float64
}

func (circle * Circle) Area() float64{
	return (math.Pi * circle.radius * circle.radius)
}

func (r * Rectangle) Area() float64{
	return (r.a * r.b)
}

func (t * Triangle) Area() float64{
	hp:=  (t.a + t.b + t.c)/2
	heron:= (hp * (hp-t.a) * (hp-t.b) * (hp-t.c))

	return (math.Sqrt(heron))
}
func (s * Square) Area() float64{
	return (s.a * s.a)
}

func (item ParentShape) Area() (float64, error) {
	switch item.Type {
	case "circle":
		R, err := Parse(item.R)
		if err != nil {
			return 0, err
		}
		circle := Circle{radius: R}
		return circle.Area(), nil
	case "square":
		A, err := Parse(item.A)
		if err != nil {
			return 0, err
		}
		square := Square{a:A}
		return square.Area(), nil
	case "rectangle":
		A, err := Parse(item.A)
		if err != nil {
			return 0, err
		}
		B, err := Parse(item.B)
		if err != nil {
			return 0, err
		}
		rectangle := Rectangle{a:A, b:B}
		return rectangle.Area(), nil
	case "triangle":
		A, err := Parse(item.A)
		if err != nil {
			return 0, err
		}
		B, err := Parse(item.B)
		if err != nil {
			return 0, err
		}
		C, err := Parse(item.C)
		if err != nil {
			return 0, err
		}
		triangle := Triangle{a:A, b:B, c:C}
		return triangle.Area(), nil


	default:
		return 0, errors.New("default")
	}

}


func main() {
	figures := "figures.json"
	dat, err := ioutil.ReadFile(figures)
	if err != nil {
		fmt.Println(err)
	}

	shapes := Shapes{}
	err = json.Unmarshal(dat, &shapes)
	if err != nil {
		fmt.Println(err)
	}

	for _, s := range shapes.Shapes {
		area, err := s.Area()
		if err != nil {
			fmt.Println("OOPs")
		} else {
			ans, _ := json.Marshal(s)
			fmt.Println(string(ans), area)
		}
	}

}
